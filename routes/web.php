<?php
namespace App\Http\Controllers;

// use App\Http\Controllers\Auth\UserController;

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Models\Element;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome');
});
Route::post('/kaldik', [KaldikController::class, 'index'])->name('kaldik.index');

Route::get('/login', function() {
    return redirect('/');
});

Route::post('/login', [Auth\UserController::class, 'login'])->name('login');

Route::group(['middleware' => 'auth'], function() {

    Route::inertia('/about', 'About')->middleware('auth')->name('about');

    Route::inertia('/beranda', 'Dashboard/Home')->middleware('auth')->name('beranda');

    Route::prefix('sekolah')->group(function() {
        Route::inertia('/', 'Dashboard/Sekolah')->name('sekolah');
        Route::post('/', [SekolahController::class, 'index'])->name('sekolah.index');
        Route::post('/store', [SekolahController::class, 'store'])->name('sekolah.store');
        Route::post('/impor', [SekolahController::class, 'impor'])->name('sekolah.impor');
        Route::delete('/{id}', [SekolahController::class, 'destroy'])->name('sekolah.destroy');
    });

    Route::prefix('guru')->group(function() {
        Route::inertia('/', 'Dashboard/Guru')->name('guru');
        Route::post('/', [GuruController::class, 'index'])->name('guru.index');
        Route::post('/store', [GuruController::class, 'store'])->name('guru.store');
        Route::post('/create-account', [GuruController::class, 'createAccount'])->name('guru.create-account');
        Route::post('/impor', [GuruController::class, 'impor'])->name('guru.impor');
        Route::delete('/{id}', [GuruController::class, 'destroy'])->name('guru.destroy');
        Route::post('/{id}/remove-account', [GuruController::class, 'removeAccount'])->name('guru.remove-account');
    });
// Master
Route::inertia('/kaldik', 'Dashboard/Kaldik')->name('kaldik.admin');

    
// Rencana
    Route::prefix('rencana')->group(function() {
        Route::inertia('/', 'Dashboard/Rencana/Index')->name('rencana');

        Route::prefix('kaldik')->group(function() {
            Route::inertia('/', 'Dashboard/Kaldik')->name('kaldik');
            
            Route::post('/store', [KaldikController::class, 'store'])->name('kaldik.store');
            Route::delete('/{id}', [KaldikController::class, 'destroy'])->name('kaldik.destroy');
        });

        Route::prefix('jadwal')->group(function () {
            Route::inertia('/','Dashboard/Jadwal')->name('jadwal');
            Route::post('/', [JadwalController::class, 'index'])->name('jadwal.index');
            Route::delete('/{id}', [JadwalController::class, 'destroy'])->name('jadwal.destroy');
            Route::put('/{id}', [JadwalController::class, 'update'])->name('jadwal.update');
            Route::post('/store', [JadwalController::class, 'store'])->name('jadwal.store');
        });

        Route::prefix('prota')->group(function() {
            // Route::inertia('/', 'Dashboard/Prota')->name('prota.page');
            Route::post('/', [ProtaController::class, 'index'])->name('prota.index');
            Route::post('/store', [ProtaController::class, 'store'])->name('prota.store');
        });

        Route::prefix('prosem')->group(function() {
            Route::post('/', [ProsemController::class, 'index'])->name('prosem.index');
            Route::post('/store', [ProsemController::class, 'store'])->name('prosem.store');
        });

        Route::prefix('pembelajaran')->group(function() {
            Route::post('/', [PembelajaranController::class, 'index'])->name('pembelajaran.index');
        });
    });

    

    


    Route::prefix('periode')->group(function() {
        Route::inertia('/', 'Dashboard/Periode')->name('periode');
        Route::post('/', [PeriodeController::class, 'index'])->name('periode.index');
        Route::post('/store', [PeriodeController::class, 'store'])->name('periode.store');
        Route::put('/{id}', [PeriodeController::class, 'toggle'])->name('periode.toggle');
    });

    Route::prefix('rombel')->group(function() {
        Route::inertia('/', 'Dashboard/Rombel')->name('rombel');
        Route::post('/', [RombelController::class, 'index'])->name('rombel.index');
        Route::post('/store', [RombelController::class, 'store'])->name('rombel.store');
        Route::post('/impor-siswa', [RombelController::class, 'imporSiswa'])->name('rombel.impor-siswa');
        Route::delete('/{id}', [RombelController::class, 'destroy'])->name('rombel.destroy');
    });
    Route::prefix('siswa')->group(function() {
        Route::get('/', [SiswaController::class, 'viewSiswa'])->name('siswa');
        Route::delete('/by-rombel/{rombel}', [SiswaController::class, 'destroyByRombel'])->name('siswa.delete.by-rombel');
    });

    

    Route::prefix('proses')->group(function() {
        Route::inertia('/', 'Dashboard/Proses/Index')->name('proses.page');
    });
    
    Route::prefix('evaluasi')->group(function() {
        Route::inertia('/', 'Dashboard/Evaluasi/Index')->name('evaluasi.page');
    });

    Route::prefix('profil')->group(function() {
        Route::inertia('/', 'Dashboard/Profil')->name('profil');
    });

    Route::prefix('setting')->group(function() {
        Route::get('/', [SettingController::class, 'page'])->name('setting.page');
    });

    Route::get('/logout', function() {
        auth()->logout();
        return redirect('/');
    })->name('logout');

});