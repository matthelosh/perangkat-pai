import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue2 from '@vitejs/plugin-vue2';
import Components from 'unplugin-vue-components/vite';
import { VuetifyResolver } from 'unplugin-vue-components/resolvers'
import path from 'path';

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/js/app.js'],
            refresh: true,
        }),
        vue2(),
        Components({
            resolvers: [VuetifyResolver()],
        })
    ],
    resolve: {
        alias: {
            '@': '/resources/js',
            'ziggy': path.resolve('vendor/tightenco/ziggy/dist/vue.es.js'),
        }
    },
    build: {
        rollupOptions: {
            output: {
                assetFileNames: (assetInfo) => {
                    return assetInfo.name == 'app.css' ? 'assets/app.css' : 'assets/'+assetInfo.name;
                }
            }
        }
    }
});
