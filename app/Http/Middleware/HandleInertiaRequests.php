<?php

namespace App\Http\Middleware;

use App\Models\Kaldik;
use Illuminate\Http\Request;
use Inertia\Middleware;
use App\Models\Periode;
use App\Models\Menu;
use App\Models\Sekolah;
use App\Models\Rombel;
use Illuminate\Database\Query\Builder;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request): ?string
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request): array
    {
        return array_merge(parent::share($request), [
            'user' => $request->user() ?? null,
            'menus' => $this->menus($request->user()) ?? null,
            'periode' => $this->periode() ?? null,
            'sekolahs' => $request->user() ? $this->sekolahs($request->user()) : null,
            'rombels' => $request->user() ? $this->rombels($request->user(), $this->periode()) : null,
            'app_env' => $request->user() ? config('app.env') : null,
        ]);
    }

    public function rombels($user, $periode)
    {
        // return $user->guru->id;
        if(!$periode) {
            return null;
        }
       return Rombel::where([
        ['guru_id','=',$user->guru->id],
        ['periode_id','=',$periode->kode_periode]
       ])->has('jadwals')->with('jadwals','siswas')->get();
    }
    public function periode()
    {
       return Periode::where('is_active', 1)->first() ?? null;
    }

    private function menus($user) {
        $menus = $user ? Menu::where('parent_id',0)->where(function($query) use ($user) {
            $query->where('roles','all')->orWhere('roles', $user->level);
        })->with('children', function($q) use ($user) {
            $q->where('roles', 'all');
            $q->orWhere('roles', $user->level);
        })->get() : [];
        return $menus;
    }

    private function sekolahs($user) {
        if ($user->level == 'gpai') {
            return Sekolah::where('npsn', $user->guru->sekolah_id)->get();
        } else {
            return Sekolah::all();
        }
    }
}
