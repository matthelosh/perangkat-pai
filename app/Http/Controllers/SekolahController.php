<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Stmt\TryCatch;

class SekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            if ($request->user()->level === 'gpai') {
                $sekolahs = Sekolah::where('npsn', $request->user()->guru->sekolah_id)->get();
            } else {
                $sekolahs = Sekolah::all();
            }
            return response()->json(['status' => 'success', 'sekolahs' => $sekolahs], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = json_decode($request->sekolah);
        try {
            if ($request->file('logo')) {
                Storage::putFileAs('public/img/logo/', $request->logo, $data->npsn.'.png');
            }

            $sekolah = Sekolah::updateOrCreate(
                [
                    'id' => $data->id ?? null,
                    'npsn' => $data->npsn ?? null,
                ],
                [
                    'name' => $data->name ?? null,
                    'alamat' => $data->alamat ?? null,
                    'rt' => $data->rt ?? null,
                    'rw' => $data->rw ?? null,
                    'desa' => $data->desa ?? null,
                    'kecamatan' => $data->kecamatan ?? 'WAGIR',
                    'kabupaten' => $data->kabupaten ?? 'MALANG',
                    'kode_pos' => $data->kode_pos ?? '65158',
                    'telp' => $data->telp ?? '-',
                    'email' => $data->email ?? null,
                    'website' => $data->website ?? '-',
                    'nama_ks' => $data->nama_ks ?? null,
                    'nip_ks' => $data->nip_ks ?? '-'
                ]
            );
            return response()->json(['status' => 'success', 'data' => $sekolah], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'data' => $e->getMessage()], 500);
        }
    }

    public function impor(Request $request)
    {
        try {
            $datas = $request->all();
            foreach ($datas as $data) {
                Sekolah::updateOrCreate(
                    [
                        'id' => $data['id'] ?? null,
                        'npsn' => $data['npsn'] ?? null,
                    ],
                    [
                        'name' => $data['name'] ?? null,
                        'alamat' => $data['alamat'] ?? null,
                        'rt' => $data['rt'] ?? null,
                        'rw' => $data['rw'] ?? null,
                        'desa' => $data['desa'] ?? null,
                        'kecamatan' => $data['kecamatan'] ?? 'WAGIR',
                        'kabupaten' => $data['kabupaten'] ?? 'MALANG',
                        'kode_pos' => $data['kode_pos'] ?? '65158',
                        'telp' => $data['telp'] ?? '-',
                        'email' => $data['email'] ?? null,
                        'website' => $data['website'] ?? '-',
                        'nama_ks' => $data['nama_ks'] ?? null,
                        'nip_ks' => $data['nip_ks'] ?? '-'
                    ]
                );
            }
            return response()->json(['status' => 'success', 'msg' => 'Data Tersimpan'], 200);
        } catch(\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Sekolah $sekolah, $id)
    {
        try {
            $hapus = Sekolah::find($id)->delete();
            return response()->json(['status' => 'success'], 200);
        } catch(\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }
}
