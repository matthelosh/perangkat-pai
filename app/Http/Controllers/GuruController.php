<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            return response()->json(['status' => 'success', 'gurus' => Guru::with('user', 'sekolah')->get()], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = json_decode($request->guru);
        // dd($data);
        try {
            $guru = Guru::updateOrCreate(
                [
                    'id' => $data->id ?? null,
                    
                ],
                [
                    'nip' => $data->nip ?? null,
                    'nuptk' => $data->nuptk ?? null,
                    'fullname' => $data->fullname ?? null,
                    'jk' => $data->jk ?? null,
                    'hp' => $data->hp ?? '-',
                    'status' => $data->status ?? null,
                    'sertifikasi' => $data->sertifikasi ?? '-',
                    'alamat' => $data->alamat ?? 'Malang',
                    'sekolah_id' => $data->sekolah_id ?? null,
                ]
            );
            if ($request->file('foto')) {
                Storage::putFileAs('public/img/guru/', $request->foto, $guru->id.'.jpg');
            }
            return response()->json(['status' => 'success', 'data' => $guru], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'data' => $e->getMessage()], 500);
        }
    }

    public function createAccount(Request $request)
    {
        try {
            $user = User::updateOrCreate(
                [
                    'id' => $request->id ?? null
                ],
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'level' => $request->level,
                    'guru_id' => $request->guru_id
                ]
            );

            return response()->json(['status' => 'success', 'user' => $user], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }

    public function removeAccount(Request $request, $id) {
        try {
            $guru = Guru::findOrFail($id);
            $account = $guru->user()->delete();

            return response()->json(['status' => 'success', 'msg' => 'Akun dihapus'], 200);
        } catch(\Exception $e) {
            dd($e);
        }
    }

    public function impor(Request $request)
    {
        try {
            $datas = $request->all();
            foreach($datas as $data) {
                Guru::updateOrCreate(
                    [
                        'id' => $data['id'] ?? null,
                        
                    ],
                    [
                        'nip' => $data['nip'] ?? null,
                        'nuptk' => $data['nuptk'] ?? null,
                        'fullname' => $data['fullname'] ?? null,
                        'jk' => $data['jk'] ?? null,
                        'hp' => $data['hp'] ?? '-',
                        'status' => $data['status'] == '0' ? 0 : 1,
                        'sertifikasi' => $data['sertifikasi'] ?? '-',
                        'alamat' => $data['alamat'] ?? 'Malang',
                        'sekolah_id' => $data['sekolah_id'] ?? null,
                    ]
                );
            }
            return response()->json(['status' => 'success', 'msg' => 'Data Guru Diimpor'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Guru $guru, $id)
    {
        try {
            $guru = Guru::findOrFail($id);
            $guru->user()->delete();
            $guru->delete();
            return response()->json(['status' => 'success'], 200);
        } catch(\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }
}
