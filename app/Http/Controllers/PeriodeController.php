<?php

namespace App\Http\Controllers;

use App\Models\Periode;
use Illuminate\Http\Request;

class PeriodeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            return response()->json(['status' => 'success', 'periodes' => Periode::all()], 200);
        } catch(\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $periode = Periode::updateOrCreate(
                [
                    'id' => $request->id ?? null
                ],
                [

                    'kode_periode' => $request->kode_periode,
                    'tapel' => $request->tapel,
                    'semester' => $request->semester,
                    'deskripsi' => $request->deskripsi,
                    'start' => $request->start,
                    'end' => $request->end,
                ]
            );

            return response()->json(['status' => 'success', 'periode' => $periode], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }

    public function toggle(Request $request, $id)
    {
        try {
            Periode::where('is_active', 1)->update(['is_active' => 0]);
            $periode = Periode::find($id)->update(['is_active' => 1]);
            // $periode->update(['is_active' => 1]);
            // $periode->update(['active' => 1]);
            return response(['status' => 'success', 'periode' => $periode]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Periode $periode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Periode $periode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Periode $periode)
    {
        //
    }
}
