<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Inertia\Inertia;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function page() {

        return Inertia::render('Dashboard/Setting', [
            'menuItems' => Menu::with('parent', 'children')->get(),
        ]);
    }
}
