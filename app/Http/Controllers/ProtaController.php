<?php

namespace App\Http\Controllers;

use App\Models\Cp;
use App\Models\Prota;
use App\Models\Pembelajaran;
use Illuminate\Http\Request;

class ProtaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $kodeRombel = $request->query('rombel');
            $kode_rombel = explode("-", $request->query('rombel'));
            $rombel = end($kode_rombel);
            $tingkat = substr($rombel,0,1);
            // dd($tingkat);
            $pembelajarans = Pembelajaran::where('tingkat', $tingkat)->with('materis.protas', function($q) use($kodeRombel) {
                $q->where('protas.rombel_id', $kodeRombel);
            })->get();
            $fase = intval($tingkat) >= 5 ? 'C' : (intval($tingkat) >= 3 ? 'B' : 'A');
            $cps = Cp::where('fase', $fase)->get();
            $protas = Prota::where('rombel_id', $request->query('rombel'))->with('pembelajaran.materis', 'materi')->get();
            

            return response()->json([
                'status' => 'success',
                'pembelajarans' => $pembelajarans,
                'cps' => $cps,
                'protas' => $protas,
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            
            foreach($request->data as $k => $v) {
                $kode = explode("-",$k);
                $pembelajaran_id = $kode[0]."-".$kode[1]."-".$kode[2]."-".$kode[3];
                Prota::updateOrCreate(
                    [
                        'materi_id' => $k,
                        'pembelajaran_id' => $pembelajaran_id,
                        'rombel_id' => $request->rombel_id
                    ],
                    [
                        'aw' => $v
                    ]
                );
            }

            return response()->json(['status' => 'success', 'msg' => 'Prota disimpan'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Prota $prota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Prota $prota)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Prota $prota)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Prota $prota)
    {
        //
    }
}
