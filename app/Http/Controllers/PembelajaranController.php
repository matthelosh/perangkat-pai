<?php

namespace App\Http\Controllers;

use App\Models\Cp;
use App\Models\Prota;
use App\Models\Pembelajaran;
use Illuminate\Http\Request;

class PembelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $kodeRombel = $request->query('rombel');
            $kode_rombel = explode("-", $request->query('rombel'));
            $rombel = end($kode_rombel);
            $tingkat = substr($rombel,0,1);
            // dd($tingkat);
            $pembelajarans = Pembelajaran::where('tingkat', $tingkat)->with('materis.protas', function($q) use($kodeRombel) {
                $q->where('protas.rombel_id', $kodeRombel);
            })->with('materis.prosems', function($q) use($kodeRombel) {
                $q->where('prosems.rombel_id', $kodeRombel);
            })->get();
            $fase = intval($tingkat) >= 5 ? 'C' : (intval($tingkat) >= 3 ? 'B' : 'A');
            $cps = Cp::where('fase', $fase)->get();
            $protas = Prota::where('rombel_id', $request->query('rombel'))->get();
            

            return response()->json([
                'status' => 'success',
                'pembelajarans' => $pembelajarans,
                'cps' => $cps,
                'protas' => $protas,
            ], 200);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Pembelajaran $pembelajaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pembelajaran $pembelajaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pembelajaran $pembelajaran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pembelajaran $pembelajaran)
    {
        //
    }
}
