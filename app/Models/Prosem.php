<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prosem extends Model
{
    use HasFactory;

    protected $fillable = [
        'guru_id',
        'pembelajaran_id',
        'materi_id',
        'rombel_id',
        'tanggal',
        'minggu_ke',
        'hari'
    ];

    public function rombel() {
        return $this->belongsTo(Rombel::class, 'rombel_id', 'kode_rombel');
    }

    public function materi() {
        return $this->belongsTo(Materi::class, 'materi_id', 'kode');

    }

}
