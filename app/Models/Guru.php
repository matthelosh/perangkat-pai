<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    use HasFactory;
    protected $fillable = [
        'nip',
        'nuptk',
        'fullname',
        'jk',
        'hp',
        'alamat',
        'status',
        'sertifikasi',
        'sekolah_id'
    ];

    public function sekolah()
    {
        return $this->belongsTo(Sekolah::class, 'sekolah_id', 'npsn');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'guru_id', 'id');
    }
}
