<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;

    protected $fillable = [
        'nisn',
        'nama',
        'jk',
        'sekolah_id',
        'rombel_id'
    ];

    public function rombel()
    {
        return $this->belongsTo(Rombel::class, 'rombel_id', 'kode_rombel');
    }

    public function sekolah()
    {
        return $this->belongsTo(Sekolah::class, 'sekolah_id','npsn');
    }
}
