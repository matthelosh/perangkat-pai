<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prota extends Model
{
    use HasFactory;
    protected $fillable = [
        'rombel_id',
        'pembelajaran_id',
        'materi_id',
        'aw'  
    ];

    public function pembelajaran() {
        return $this->belongsTo(Pembelajaran::class, 'pembelajaran_id', 'kode');
    }

    public function materi() {
        return $this->belongsTo(Materi::class, 'materi_id', 'kode');
    }
}
