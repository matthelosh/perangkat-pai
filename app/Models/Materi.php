<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    use HasFactory;

    protected $fillable = [
        'kode',
        'pembelajaran_id',
        'label',
        'description'
    ];

    public function pembelajaran() {
        return $this->belongsTo(Pembelajaran::class, 'pembelajaran_id', 'kode');
    }

    public function protas() {
        return $this->hasMany(Prota::class, 'materi_id', 'kode');
    }

    public function prosems() {
        return $this->hasMany(Prosem::class, 'materi_id', 'kode');
    }

}
