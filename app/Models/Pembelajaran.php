<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembelajaran extends Model
{
    use HasFactory;
    protected $fillable = [
        'kode',
        'fase',
        'tingkat',
        'label',
        'description'
    ];


    public function materis() {
        return $this->hasMany(Materi::class, 'pembelajaran_id', 'kode');
    }

    

}
