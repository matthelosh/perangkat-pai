<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rombel extends Model
{
    use HasFactory;

    protected $fillable = [
        'kode_rombel',
        'label',
        'tingkat',
        'sekolah_id',
        'guru_id',
        'periode_id',
        'kurikulum',
        'fase'
    ];

    public function periode()
    {
        return $this->belongsTo(Periode::class, 'periode_id','kode_periode');
    }
    public function sekolah()
    {
        return $this->belongsTo(Sekolah::class, 'sekolah_id', 'npsn');
    }

    public function guru()
    {
        return $this->belongsTo(Guru::class, 'guru_id', 'id');
    }

    public function siswas()
    {
        return $this->hasMany(Siswa::class, 'rombel_id', 'kode_rombel');
    }

    public function jadwals()
    {
        return $this->hasMany(Jadwal::class, 'rombel_id', 'kode_rombel');
    }

    public function pembelajarans() {
        return $this->hasMany(Pembelajaran::class, 'tingkat', 'tingkat');
    }
}
