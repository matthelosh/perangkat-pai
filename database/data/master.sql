INSERT INTO `menus`(`id`,`label`,`url`,`icon`,`parent_id`,`roles`,`created_at`,`updated_at`) VALUES 
( '1', 'Beranda', '/beranda', 'mdi-home', '0', 'all', NULL, NULL ),
( '2', 'Utama', '#', 'mdi-database', '0', 'all', NULL, NULL ),
( '3', 'Data Sekolah', '/sekolah', 'mdi-city', '2', 'all', NULL, NULL ),
( '4', 'Guru', '/guru', 'mdi-account-tie', '2', 'admin', NULL, NULL ),
( '5', 'Rombel', '/rombel', 'mdi-google-classroom', '2', 'all', NULL, NULL ),
( '6', 'Siswa', '/siswa', 'mdi-human-child', '2', 'all', NULL, NULL ),
( '7', 'Periode', '/periode', 'mdi-calendar', '2', 'admin', NULL, NULL ),
( '8', 'Perangkat', '#', 'mdi-book', '0', 'gpai', NULL, NULL ),
( '9', 'Rencana', '/rencana', 'mdi-pencil', '8', 'gpai', NULL, NULL ),
( '10', 'Kaldik', '/kaldik', 'mdi-calendar', '8', 'gpai', NULL, NULL ),
( '11', 'Jadwal', '/jadwal', 'mdi-calendar', '8', 'gpai', NULL, NULL ),
( '12', 'Prota', '/prota', 'mdi-calendar', '8', 'gpai', '2024-01-06 07:00:00', '2024-01-06 07:00:00' );