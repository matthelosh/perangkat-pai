INSERT INTO `cps`(`id`,`fase`,`elemen`,`teks`,`created_at`,`updated_at`,`kode`) VALUES 
( '1', 'A', 'Al-Quran dan
Hadis', 'Pendidikan Agama Islam dan Budi Pekerti menekankan kemampuan mengenal huruf hijaiah dan harakatnya, huruf hijaiah bersambung, dan kemampuan membaca surah-surah pendek Al-Qur�an dengan baik', NULL, NULL, 'qh-a' ),
( '2', 'A', 'Akidah', 'Peserta didik mengenal rukun iman kepada Allah melalui nama-namanya yang agung (asmaulhusna) dan mengenal para malaikat dan tugas yang diembannya.', NULL, NULL, 'aq-a' ),
( '3', 'A', 'Akhlak', 'Peserta didik terbiasa mempraktikkan nilai-nilai baik dalam kehidupan sehari-hari dalam ungkapan-ungkapan positif baik untuk dirinya maupun sesama manusia, terutama orang tua
dan guru. Peserta didik juga memahami pentingnya tradisi memberi dalam ajaran agama Islam. Mereka mulai mengenal norma yang ada di lingkungan sekitarnya. Peserta didik juga
terbiasa percaya diri mengungkapkan pendapat pribadinya dan belajar menghargai pendapat yang berbeda. Peserta didik juga terbiasa melaksanakan tugas kelompok serta memahami
pentingnya mengenali kekurangan diri dan kelebihan temannya demi terwujudnya suasana saling mendukung satu sama lain.', NULL, NULL, 'ak-a' ),
( '4', 'A', 'Fikih', 'Peserta didik mampu mengenal rukun Islam dan kalimah syahadatain, menerapkan tata cara bersuci, salat fardu, azan, ikamah, zikir dan berdoa setelah salat.', NULL, NULL, 'fq-a' ),
( '5', 'A', 'Sejarah Peradaban
Islam', 'Peserta didik mampu menceritakan secara sederhana kisah beberapa nabi yang wajib diimani.', NULL, NULL, 'sp-a' ),
( '6', 'B', 'Al-Quran dan
Hadis', 'Peserta didik mampu membaca surah-surah pendek atau ayat Al-Qur�an dan menjelaskan pesan pokoknya dengan baik. Peserta didik mengenal hadis tentang kewajiban salat dan menjaga hubungan baik dengan sesama serta mampu menerapkan dalam kehidupan seharihari.', NULL, NULL, 'qh-b' ),
( '7', 'B', 'Akidah', 'Peserta didik memahami sifat-sifat bagi Allah, beberapa asmaulhusna, mengenal kitab-kitab Allah, para nabi dan rasul Allah yang wajib diimani', NULL, NULL, 'aq-b' ),
( '8', 'B', 'Akhlak', 'Pada elemen akhlak, peserta didik menghormati dan berbakti kepada orang tua dan guru, dan menyampaikan ungkapan-ungkapan positif (kalimah ?ayyibah) dalam keseharian. Peserta didik memahami arti keragaman sebagai sebuah ketentuan dari Allah Swt. (sunnatull?h). Peserta didik mengenal norma yang ada di lingkungan sekitarnya dan lingkungan yang lebih luas, percaya diri mengungkapkan pendapat pribadi, memahami pentingnya musyawarah untuk mencapai kesepakatan dan pentingnya persatuan.', NULL, NULL, 'ak-b' ),
( '9', 'B', 'Fikih', 'Pada elemen fikih, peserta didik dapat melaksanakan puasa, salat jumat dan salat sunah dengan baik, memahami konsep balig dan tanggung jawab yang menyertainya (takl?f)', NULL, NULL, 'fq-b' ),
( '10', 'B', 'Sejarah Peradaban
Islam', 'Dalam pemahamannya tentang sejarah, peserta didik mampu menceritakan kondisi Arab pra Islam, masa kanak-kanak dan remaja Nabi Muhammad saw. hingga diutus menjadi rasul, berdakwah, hijrah dan membangun Kota Madinah.', NULL, NULL, 'sp-b' ),
( '11', 'C', 'Al-Quran dan
Hadis', 'Peserta didik mampu membaca, menghafal, menulis, dan memahami pesan pokok surahsurah pendek dan ayat Al-Qur�an tentang keragaman dengan baik dan benar', NULL, NULL, 'qh-c' ),
( '12', 'C', 'Akidah', 'Peserta didik dapat mengenal Allah melalui asmaulhusna, memahami keniscayaan peritiwa hari akhir, qad?? dan qadr', NULL, NULL, 'aq-c' ),
( '13', 'C', 'Akhlak', 'Peserta didik mengenal dialog antar agama dan kepercayaan dan menyadari peluang dan tantangan yang bisa muncul dari keragaman di Indonesia. Peserta didik memahami arti ideologi secara sederhana dan pandangan hidup dan memahami pentingnya menjaga kesatuan atas keberagaman. Peserta didik juga memahami pentingnya introspeksi diri untuk menjadi pribadi yang lebih baik setiap harinya. Peserta didik memahami pentingnya pendapat yang logis, menerima perbedaan pendapat, dan menemukan titik kesamaan (kalimah saw??) untuk mewujudkan persatuan dan kerukunan. Peserta didik memahami peran manusia sebagai khalifah Allah di bumi untuk menebarkan kasih sayang dan tidak membuat kerusakan di muka bumi.', NULL, NULL, 'ak-c' ),
( '14', 'C', 'Fikih', 'Pada elemen fikih, peserta didik mampu memahami zakat, infak, sedekah dan hadiah, memahami ketentuan haji, halal dan haram serta mempraktikkan puasa sunnah', NULL, NULL, 'fq-c' ),
( '15', 'C', 'Sejarah Peradaban
Islam', 'Pada elemen sejarah, peserta didik menghayati ibrah dari kisah Nabi Muhammad saw. di masa separuh akhir kerasulannya serta kisah alkhulaf?al-r?syid?n.', NULL, NULL, 'sp-c' );