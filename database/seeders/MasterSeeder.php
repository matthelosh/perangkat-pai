<?php

namespace Database\Seeders;

use App\Models\Cp;
use App\Models\p5;
use App\Models\Guru;
use App\Models\Kaldik;
use App\Models\Materi;
use App\Models\Menu;
use App\Models\Sekolah;
use App\Models\Pembelajaran;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;

class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Menu::truncate();
        Guru::truncate();
        Sekolah::truncate();
        Pembelajaran::truncate();
        p5::truncate();
        Cp::truncate();
        Materi::truncate();
        Kaldik::truncate();

        $sekolahs = file_get_contents(database_path().'/data/sekolahs.sql');
        DB::statement($sekolahs);
        $menu = file_get_contents(database_path().'/data/menu.sql');
        DB::statement($menu);
        $cps = file_get_contents(database_path().'/data/cps.sql');
        DB::statement($cps);
        $gurus = file_get_contents(database_path().'/data/gurus.sql');
        DB::statement($gurus);
        $pembelajarans = file_get_contents(database_path().'/data/pembelajarans.sql');
        DB::statement($pembelajarans);
        $materis = file_get_contents(database_path().'/data/materis.sql');
        DB::statement($materis);
        $p5 = file_get_contents(database_path().'/data/p5.sql');
        DB::statement($p5);
        $kaldiks = file_get_contents(database_path().'/data/kaldiks.sql');
        DB::statement($kaldiks);
    }
}
