<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('protas', function (Blueprint $table) {
            $table->id();
            $table->string('rombel_id', 50);
            $table->string('pembelajaran_id', 50);
            $table->string('materi_id', 50);
            $table->integer('aw');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('protas');
    }
};
