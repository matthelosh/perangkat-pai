<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sekolahs', function (Blueprint $table) {
            $table->id();
            $table->string('npsn', 10)->unique();
            $table->string('name', 100);
            $table->string('alamat', 100);
            $table->string('rt',3)->nullable();
            $table->string('rw', 3)->nullable();
            $table->string('desa', 60)->nullable();
            $table->string('kecamatan', 30)->default('Wagir');
            $table->string('kabupaten', 40)->default('Malang');
            $table->string('kode_pos', 5)->default('65158');
            $table->string('telp', 15)->default('0855555555');
            $table->string('email', 100)->nullable();
            $table->string('website', 191)->nullable();
            $table->string('nama_ks', 60)->nullable();
            $table->string('nip_ks', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sekolahs');
    }
};
