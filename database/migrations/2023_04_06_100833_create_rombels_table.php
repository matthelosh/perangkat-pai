<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rombels', function (Blueprint $table) {
            $table->id();
            $table->string('kode_rombel', 60)->unique();
            $table->string('label', 191);
            $table->string('tingkat', 1);
            $table->string('sekolah_id', 10);
            $table->integer('guru_id');
            $table->string('periode_id', 10);
            $table->enum('kurikulum', ['merdeka', 'k13'])->default('merdeka');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rombels');
    }
};
