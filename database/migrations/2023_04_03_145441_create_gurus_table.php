<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gurus', function (Blueprint $table) {
            $table->id();
            $table->string('nip')->nullable()->unique();
            $table->string('nuptk')->nullable()->unique();
            $table->string('fullname', 60);
            $table->enum('jk', ['Laki-laki','Perempuan']);
            $table->string('hp', 15);
            $table->string('alamat', 191);
            $table->enum('status', ['pns', 'gtt', 'p3k']);
            $table->boolean('sertifikasi')->default(0);
            $table->string('sekolah_id', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gurus');
    }
};
