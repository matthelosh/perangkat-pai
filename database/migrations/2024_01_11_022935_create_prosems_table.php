<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('prosems', function (Blueprint $table) {
            $table->id();
            $table->string('materi_id', 50);
            $table->string('guru_id', 50);
            $table->string('pembelajaran_id', 50);
            $table->string('rombel_id', 50);
            $table->date('tanggal');
            $table->string('minggu_ke', 3);
            $table->string('hari', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prosems');
    }
};
