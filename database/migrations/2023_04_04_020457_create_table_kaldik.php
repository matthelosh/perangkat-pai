<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kaldiks', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('description', 191);
            $table->date('start');
            $table->date('end');
            $table->string('location', 100)->nullable();
            $table->string('color', 30)->default('purple');
            $table->string('user_id', 30);
            $table->boolean('is_libur')->default(1);
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kaldiks');
    }
};
