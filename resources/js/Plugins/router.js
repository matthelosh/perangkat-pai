import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/home",
            component: () => import("@/Pages/Home.vue"),
        },
        {
            path: '*',
            component: {
                tempate: '<h1>Not Found</h1>'
            }
        }
    ],
    scrollBehavior (to, from, savedPosition) {
        if (to.hash) {
            return {
                selector: to.hash,
                behavior: 'smooth'
            }
        }
    }
});

router.beforeEach((t, from, next) => {

});

export default router;