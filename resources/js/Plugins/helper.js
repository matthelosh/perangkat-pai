export default {
    capitalize: (s) => s && s.length > 0 && s[0].toUpperCase() + s.slice(1),
    uppercase: (s) => s.toUpperCase(),
    
}

