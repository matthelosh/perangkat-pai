import Vue from 'vue'
import "@mdi/font/css/materialdesignicons.css";
// import "vuetify/src/styles/styles.sass";
import 'vuetify/dist/vuetify.min.css'

import Vuetify from "vuetify";

Vue.use(Vuetify)
export default new Vuetify({
  theme: {
    themes: {
      light: {
        background: "#15202b",
        surface: "#15202b",
        primary: "#3f51b5",
        secondary: "#03dac6",
        error: "#f44336",
        info: "#2196F3",
        success: "#4caf50",
        warning: "#fb8c00",
      },
      dark: {
        background: "#15202b",
        surface: "#15202b",
        primary: '#00bcd4',
        secondary: '#3f51b5',
        accent: '#607d8b',
        error: '#f44336',
        warning: '#ff9800',
        info: '#03a9f4',
        success: '#8bc34a'
      },
    }
  },
  icons: {
    iconfont: 'mdiSvg'
  }
});