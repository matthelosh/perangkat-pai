import './bootstrap';
import '../css/app.css';


import Vue from 'vue';
import { createInertiaApp, Link } from '@inertiajs/vue2';
// import Vuetify from 'vuetify';
import { ZiggyVue } from 'ziggy';
import { Ziggy } from '@/ziggy';
import vuetify from '@/Plugins/vuetify';
import moment from 'moment'
// import router from '@/Plugins/router';
import helper from '@/Plugins/helper';
const helpers = {
    install() {
        Vue.helper = helper;
        Vue.prototype.$helper = helper
    }
}

moment().locale('id')
Vue.prototype.moment = moment

createInertiaApp({
    progress: {
        color: 'limegreen',
    },
    title: title => `${title} | Perangkat PAI`,
    resolve: name => {
        const pages = import.meta.glob('./Pages/**/*.vue', {eager: true})
        return pages[`./Pages/${name}.vue`]
    },
    setup({el, App, props, plugin}) {
        Vue.use(plugin)
            .use(ZiggyVue, Ziggy)
            .use(helpers)
        new Vue({
            vuetify,
            // router,
            Link,
            render: h => h(App, props),
        }).$mount(el)
    }
})